const nextTranslate = require("next-translate");

// module.exports = nextTranslate();

module.exports = {
  default: nextTranslate(),
  assetPrefix: process.env.NODE_ENV === "production" ? "/" : "",
};
