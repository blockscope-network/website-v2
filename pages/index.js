import Layout from "./components/layout/layout";

import Hero from "./components/home/hero";
// import Promotions from "./components/home/promotions";
import TimeInvestment from "./components/home/time-investment";
import Projects from "./components/home/projects";
import Technology from "./components/home/technology";
import Team from "./components/home/team";
import Faqs from "./components/home/faqs";

export default function Home() {
  return (
    <Layout className="w-full">
      <Hero />
      {/* <Promotions /> */}
      <TimeInvestment />
      <Projects />
      <Technology />
      <Team />
      <Faqs />
    </Layout>
  );
}
