import useTranslation from "next-translate/useTranslation";

export default function Footer() {
  const { t, lang } = useTranslation("layout");

  return (
    <footer className="bg-gray-800 w-full bg-hexagon-background">
      <div className="sm:container sm:px-28 py-24 mx-auto">
        <div className=" flex flex-col lg:flex-row">
          <div className="w-full lg:-mx-6 lg:w-2/5">
            <div className="px-6 text-center lg:text-left">
              <div>
                <img
                  src="/img/logos/blockscope_logo_solid_white.svg"
                  width={"150px"}
                  height={"48px"}
                />
              </div>

              <p className="sm:max-w-lg mt-2 sm:mb-0 text-gray-400 text-xs">
                {t("footer.motto")}
              </p>

              <div className="flex flex-row content-center justify-center lg:justify-start">
                <div className="flex space-x-16 lg:space-x-8 mt-6">
                  <a
                    href="https://blockscope.medium.com/"
                    className="text-gray-200 hover:text-pink-600"
                    aria-label="Medium"
                    target="_blank"
                  >
                    <svg className="w-6 h-6 fill-current" viewBox="0 0 512 512">
                      <path d="M0 32v448h448V32H0zm372.2 106.1l-24 23c-2.1 1.6-3.1 4.2-2.7 6.7v169.3c-.4 2.6.6 5.2 2.7 6.7l23.5 23v5.1h-118V367l24.3-23.6c2.4-2.4 2.4-3.1 2.4-6.7V199.8l-67.6 171.6h-9.1L125 199.8v115c-.7 4.8 1 9.7 4.4 13.2l31.6 38.3v5.1H71.2v-5.1l31.6-38.3c3.4-3.5 4.9-8.4 4.1-13.2v-133c.4-3.7-1-7.3-3.8-9.8L75 138.1V133h87.3l67.4 148L289 133.1h83.2v5z" />
                    </svg>
                  </a>
                  <a
                    href="https://gitlab.com/blockscope-network"
                    className="text-gray-200 hover:text-pink-600"
                    aria-label="Gitlab"
                    target="_blank"
                  >
                    <svg className="w-6 h-6 fill-current" viewBox="0 0 512 512">
                      <path d="M105.2 24.9c-3.1-8.9-15.7-8.9-18.9 0L29.8 199.7h132c-.1 0-56.6-174.8-56.6-174.8zM.9 287.7c-2.6 8 .3 16.9 7.1 22l247.9 184-226.2-294zm160.8-88l94.3 294 94.3-294zm349.4 88l-28.8-88-226.3 294 247.9-184c6.9-5.1 9.7-14 7.2-22zM425.7 24.9c-3.1-8.9-15.7-8.9-18.9 0l-56.6 174.8h132z" />
                    </svg>
                  </a>
                  <a
                    href="https://t.me/joinchat/12BDqxGu01kyYjU0"
                    className="text-gray-200 hover:text-pink-600"
                    aria-label="Telegram Fetch.ai"
                    target="_blank"
                  >
                    <svg className="w-6 h-6 fill-current" viewBox="0 0 512 512">
                      <path d="M446.7 98.6l-67.6 318.8c-5.1 22.5-18.4 28.1-37.3 17.5l-103-75.9-49.7 47.8c-5.5 5.5-10.1 10.1-20.7 10.1l7.4-104.9 190.9-172.5c8.3-7.4-1.8-11.5-12.9-4.1L117.8 284 16.2 252.2c-22.1-6.9-22.5-22.1 4.6-32.7L418.2 66.4c18.4-6.9 34.5 4.1 28.5 32.2z" />
                    </svg>
                  </a>
                  {/* <a
                    href="https://www.linkedin.com/in/iv%C3%A1n-mer%C3%ADn-rodr%C3%ADguez-aa030b61/"
                    className="text-gray-200 hover:text-pink-600"
                    aria-label="Linkden"
                    target="_blank"
                  >
                    <svg className="w-6 h-6 fill-current" viewBox="0 0 512 512">
                      <path d="M444.17,32H70.28C49.85,32,32,46.7,32,66.89V441.61C32,461.91,49.85,480,70.28,480H444.06C464.6,480,480,461.79,480,441.61V66.89C480.12,46.7,464.6,32,444.17,32ZM170.87,405.43H106.69V205.88h64.18ZM141,175.54h-.46c-20.54,0-33.84-15.29-33.84-34.43,0-19.49,13.65-34.42,34.65-34.42s33.85,14.82,34.31,34.42C175.65,160.25,162.35,175.54,141,175.54ZM405.43,405.43H341.25V296.32c0-26.14-9.34-44-32.56-44-17.74,0-28.24,12-32.91,23.69-1.75,4.2-2.22,9.92-2.22,15.76V405.43H209.38V205.88h64.18v27.77c9.34-13.3,23.93-32.44,57.88-32.44,42.13,0,74,27.77,74,87.64Z" />
                    </svg>
                  </a> */}
                  {/* <a
                    href="#"
                    className="text-gray-200 hover:text-pink-600"
                    aria-label="Twitter"
                  >
                    <svg className="w-6 h-6 fill-current" viewBox="0 0 512 512">
                      <path d="M496,109.5a201.8,201.8,0,0,1-56.55,15.3,97.51,97.51,0,0,0,43.33-53.6,197.74,197.74,0,0,1-62.56,23.5A99.14,99.14,0,0,0,348.31,64c-54.42,0-98.46,43.4-98.46,96.9a93.21,93.21,0,0,0,2.54,22.1,280.7,280.7,0,0,1-203-101.3A95.69,95.69,0,0,0,36,130.4C36,164,53.53,193.7,80,211.1A97.5,97.5,0,0,1,35.22,199v1.2c0,47,34,86.1,79,95a100.76,100.76,0,0,1-25.94,3.4,94.38,94.38,0,0,1-18.51-1.8c12.51,38.5,48.92,66.5,92.05,67.3A199.59,199.59,0,0,1,39.5,405.6,203,203,0,0,1,16,404.2,278.68,278.68,0,0,0,166.74,448c181.36,0,280.44-147.7,280.44-275.8,0-4.2-.11-8.4-.31-12.5A198.48,198.48,0,0,0,496,109.5Z" />
                    </svg>
                  </a> */}
                </div>
              </div>
            </div>
          </div>

          <div className="mt-6 lg:mt-0 lg:flex-1">
            <div className="grid grid-cols-1 gap-6 sm:grid-cols-3 text-xs mt-16 lg:mt-0 lg:w-2/3 lg:float-right text-center sm:text-left">
              <div className="mb-6 sm:mb-0">
                <h3 className="uppercase text-pink-500 pb-4 font-semibold">
                  {t("footer.legal")}
                </h3>
                <a
                  href="#"
                  className="block mt-2 text-xs text-white hover:text-pink-600"
                >
                  {t("footer.cookies")}
                </a>
              </div>

              <div className="mb-6 sm:mb-0">
                <h3 className="uppercase text-pink-500 pb-4 font-semibold">
                  {t("footer.resources")}
                </h3>
                <a
                  href="https://blockscope.medium.com/"
                  target="_blank"
                  className="block mt-2 text-xs text-white hover:text-pink-600"
                >
                  {t("footer.blog")}
                </a>
                <a
                  href="https://fetch.ai/"
                  target="_blank"
                  className="block mt-4 text-xs text-white hover:text-pink-600"
                >
                  {t("footer.fetchAi")}
                </a>
                <a
                  href="https://cartesi.io/"
                  target="_blank"
                  className="block mt-4 text-xs text-white hover:text-pink-600"
                >
                  {t("footer.cartesi")}
                </a>
              </div>

              <div className="mb-6 sm:mb-0">
                <h3 className="text uppercase text-pink-500 pb-4 font-semibold">
                  Contact
                </h3>
                <span className="block mt-4 text-xs text-white hover:text-pink-600">
                  <a href="mailto:blockscope@protonmail.com" target="_blank">
                    {t("footer.email")}
                  </a>
                </span>
                <span className="block mt-4 text-xs text-white hover:text-pink-600">
                  <a
                    href="https://t.me/joinchat/12BDqxGu01kyYjU0"
                    target="_blank"
                  >
                    {t("footer.telegramFetchAi")}
                  </a>
                </span>
                <span className="block mt-4 text-xs text-white hover:text-pink-600">
                  <a
                    href="https://t.me/joinchat/12BDqxGu01kyYjU0"
                    target="_blank"
                  >
                    {t("footer.telegramCartesi")}
                  </a>
                </span>
                <span className="block mt-4 text-xs text-white hover:text-pink-600">
                  <a
                    href="https://gitlab.com/blockscope-network"
                    target="_blank"
                  >
                    {t("footer.gitlab")}
                  </a>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
