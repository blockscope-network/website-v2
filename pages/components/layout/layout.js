import Head from "next/head";
import useTranslation from "next-translate/useTranslation";

import Header from "./header";
import Footer from "./footer";

import styles from "./layout.module.css";

export default function Layout({ children }) {
  const { t, lang } = useTranslation("common");

  return (
    <>
      <Head>
        <title>{t("title")}</title>
        <link rel="icon" href="/img/icons/favicon.ico" />
      </Head>

      <div className={styles.container}>
        <Header />
        <main className="w-full min-h-screen">{children}</main>
        <Footer />
      </div>

      <style jsx global>
        {`
          html,
          body {
            padding: 0;
            margin: 0;
            font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
              Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
              sans-serif;
          }

          * {
            box-sizing: border-box;
          }

          a {
            color: inherit;
            text-decoration: none;
          }
        `}
      </style>
    </>
  );
}
