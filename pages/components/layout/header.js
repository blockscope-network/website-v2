import React, { useState } from "react";
import Link from "next/link";
import useTranslation from "next-translate/useTranslation";
import setLanguage from "next-translate/setLanguage";

import i18nConfig from "../../../i18n.json";
const { locales } = i18nConfig;

import styles from "./layout.module.css";

export default function Header() {
  const { t, lang } = useTranslation("layout");

  const [collapsed, setCollapsed] = useState(true);
  const collapsedClass = collapsed ? "hidden" : "";

  console.log(styles.header);

  return (
    <nav
      className={`${styles.header} fixed top-0 bg-white w-full border-b-2 border-pink-600 bg-opacity-90 z-50 h`}
    >
      <div className="px-10 py-4 lg:flex lg:justify-between lg:items-center">
        <div className="flex items-center justify-between">
          <div className="pr-8">
            <img
              src="/img/logos/blockscope_logo_letters_800.png"
              width="278px"
              alt={"Blockscope logo"}
              style={{ maxWidth: "none" }}
            />
          </div>

          <div className="flex lg:hidden">
            <div
              className={`${styles.headerFlags} flex justify-center lg:hidden mr-6 mt-1`}
            >
              <div
                className="px-4 mt-1"
                onClick={async () => await setLanguage("en")}
              >
                <img
                  src="/img/flags/gb.svg"
                  width="25px"
                  height="20px"
                  className=" cursor-pointer"
                />
              </div>
              <div
                className="px-4 mt-1"
                onClick={async () => await setLanguage("es")}
              >
                <img
                  src="/img/flags/es.svg"
                  width="25px"
                  height="20px"
                  className=" cursor-pointer"
                />
              </div>
            </div>

            <button
              type="button"
              className="text-gray-500 hover:text-gray-600 focus:outline-none focus:text-gray-600"
              aria-label="toggle menu"
              onClick={() => setCollapsed(!collapsed)}
            >
              <svg viewBox="0 0 24 24" className="w-8 h-8 fill-current">
                <path
                  fillRule="evenodd"
                  d="M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
                ></path>
              </svg>
            </button>
          </div>
        </div>

        <div className={`items-center lg:flex ${collapsedClass}`}>
          <div className="flex flex-col lg:flex-row lg:mx-18 pr-4 content-center justify-content-center text-m text-gray-700">
            <Link href="#home">
              <a
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.home")}
              </a>
            </Link>
            <Link href="#services">
              <a
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.services")}
              </a>
            </Link>
            <Link href="#calculator">
              <a
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.earnings")}
              </a>
            </Link>
            <Link href="#faq">
              <a
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.faq")}
              </a>
            </Link>
            <Link href="https://blockscope.medium.com/">
              <a
                target="_blank"
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.blog")}
              </a>
            </Link>
            <Link href="https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23">
              <a
                target="_blank"
                className="my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4"
                onClick={() => !collapsed && setCollapsed(true)}
              >
                {t("header.stake")}
              </a>
            </Link>
          </div>

          <div className="flex flex-row justify-center lg:flex hidden">
            <div
              className="px-4 mt-1"
              onClick={async () => await setLanguage("en")}
            >
              <img
                src="/img/flags/gb.svg"
                width="20px"
                height="16px"
                className=" cursor-pointer"
              />
            </div>
            <div
              className="px-4 mt-1 cursor-pointer"
              onClick={async () => await setLanguage("es")}
            >
              <img
                src="/img/flags/es.svg"
                width="20px"
                height="16px"
                className=" cursor-pointer"
              />
            </div>
          </div>
        </div>
      </div>
    </nav>
  );
}
