import { useState } from "react";
import { Fade } from "react-awesome-reveal";
import useTranslation from "next-translate/useTranslation";
import Trans from "next-translate/Trans";

import { PlusSmIcon, MinusSmIcon } from "@heroicons/react/solid";

const FaqCard = ({ question, answer, htmlInterpolations = [] }) => {
  const { t, lang } = useTranslation("home");
  const [collapsed, setCollapsed] = useState(true);

  const collapsedCssClss = collapsed ? "hidden" : "";

  const Component = (props) => (
    <Fade triggerOnce duration={400}>
      <p
        {...props}
        className={`mt-4 text-gray-500 text-justify text-sm md:text-base ${collapsedCssClss}`}
      />
    </Fade>
  );

  return (
    <div
      className="max-w-md px-8 py-8 mx-auto mt-8 bg-white rounded-lg shadow-lg 
      text-gray-600 cursor-pointer select-none w-full"
      onClick={() => setCollapsed(!collapsed)}
    >
      <div className="flex flex-row space-between content-center">
        <h2 className="my-2 mr-4 text-base font-semibold text-gray-500 hover:text-green-600 md:my-0 md:text-base flex-auto">
          {t("faqs." + question)}
        </h2>

        {collapsed ? (
          <PlusSmIcon className="h-6 mt-1" />
        ) : (
          <MinusSmIcon className="h-6 mt-1" />
        )}
      </div>

      <Trans
        i18nKey={`home:faqs.${answer}`}
        components={[<Component />, ...htmlInterpolations]}
      />
    </div>
  );
};

const GeneralFaqs = () => (
  <div className="flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn">
    <div className="lg:w-5/6">
      <FaqCard
        question="generalFaqQuestionWhatIsStaking"
        answer="generalFaqAnswerWhatIsStaking"
        htmlInterpolations={[<br />]}
      />
      <FaqCard
        question="generalFaqQuestionWhatIsDelegating"
        answer="generalFaqAnswerWhatIsDelegating"
      />
      <FaqCard
        question="generalFaqQuestionDelegatingBenefits"
        answer="generalFaqAnswerDelegatingBenefits"
      />
      <FaqCard
        question="generalFaqQuestionDelegatingRisks"
        answer="generalFaqAnswerDelegatingRisks"
        htmlInterpolations={[<br />]}
      />
    </div>
    <div className="lg:w-5/6">
      <FaqCard
        question="generalFaqQuestionHowMuchRewards"
        answer="generalFaqAnswerHowMuchRewards"
      />{" "}
      <FaqCard
        question="generalFaqQuestionCommission"
        answer="generalFaqAnswerCommision"
        htmlInterpolations={[<br />]}
      />
      <FaqCard
        question="generalFaqQuestionTypeOfInterests"
        answer="generalFaqAnswerTypeOfInterests"
        htmlInterpolations={[<br />]}
      />
    </div>
  </div>
);

const FetchAiFaqs = () => (
  <div className="flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn">
    <div className="lg:w-5/6">
      <FaqCard
        question="fetcAiFaqQuestionWhatIs"
        answer="fetcAiFaqAnswerWhatIs"
        htmlInterpolations={[<br />]}
      />
      <FaqCard
        question="fetcAiFaqQuestionMigrateTokens"
        answer="fetcAiFaqAnswerMigrateTokens"
        htmlInterpolations={[
          <br />,
          <a
            href="https://token-bridge.fetch.ai"
            target="_blank"
            className="text-green-600 hover:font-semibold"
          />,
        ]}
      />
      <FaqCard
        question="fetcAiFaqQuestionNativeTokenBenefits"
        answer="fetcAiFaqAnswerNativeTokenBenefits"
        htmlInterpolations={[<br />]}
      />
      <FaqCard
        question="fetcAiFaqQuestionTokenDifferences"
        answer="fetcAiFaqAnswerTokenDifferences"
        htmlInterpolations={[
          <br />,
          <a
            href="https://etherscan.io/address/0xaea46A60368A7bD060eec7DF8CBa43b7EF41Ad85"
            target="_blank"
            className="text-green-600 hover:font-semibold break-words"
          />,
        ]}
      />
      <FaqCard
        question="fetcAiFaqQuestionTokenUtility"
        answer="fetcAiFaqAnswerTokenUtility"
      />
      <FaqCard
        question="fetcAiFaqQuestionStakeSwapDate"
        answer="fetcAiFaqAnswerStakeSwapDate"
      />
    </div>

    <div className="lg:w-5/6">
      <FaqCard
        question="fetchAiFaqQuestionChooseValidator"
        answer="fetchAiFaqAnswerChooseValidator"
        htmlInterpolations={[
          <br />,
          <a
            href="https://explore-fetchhub.fetch.ai/validators?sort=commission&dir=1"
            target="_blank"
            className="text-green-600 hover:font-semibold"
          />,
          <strong />,
        ]}
      />
      <FaqCard
        question="generalFaqQuestionCommission"
        answer="generalFaqAnswerCommision"
        htmlInterpolations={[<br />]}
      />
      <FaqCard
        question="fetcAiFaqQuestionUnboundingPeriod"
        answer="fetcAiFaqAnswerUnboundingPeriod"
      />
      <FaqCard
        question="fetcAiFaqQuestionRedelegatingPeriod"
        answer="fetcAiFaqAnswerRedelegatingPeriod"
      />
      <FaqCard
        question="fetcAiFaqQuestionStakeSwapInfo"
        answer="fetcAiFaqAnswerStakeSwapInfo"
        htmlInterpolations={[
          <a
            href="https://fetch.ai/the-fetch-ai-stake-swap-frequently-asked-questions"
            target="_blank"
            className="text-green-600 hover:font-semibold"
          />,
        ]}
      />
    </div>
  </div>
);

const CartesiFaqs = () => (
  <div className="flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn">
    <div className="lg:w-5/6">
      <FaqCard
        question="cartesiFaqQuestionWhatIs"
        answer="cartesiFaqAnswerWhatIs"
      />
      <FaqCard
        question="cartesiFaqQuestionTokenUtility"
        answer="cartesiFaqAnswerTokenUtility"
      />
      <FaqCard
        question="cartesiFaqQuestionDelegationAvailability"
        answer="cartesiFaqAnswerDelegationAvailability"
      />
    </div>
    <div className="lg:w-5/6">
      <FaqCard
        question="cartesiFaqQuestionDelegationAvailability"
        answer="cartesiFaqAnswerDelegationAvailability"
      />
      <FaqCard
        question="cartesiFaqQuestionFurtherInfo"
        answer="cartesiFaqAnswerFurtherInfo"
      />
    </div>
  </div>
);

const FAQS = {
  general: {},
  fetchAi: {},
  cartesi: {},
};

const Button = ({ label, active, onClickHandler = () => {} }) => {
  const { t, lang } = useTranslation("home");

  const cssClasses = active
    ? "cursor-auto border focus:ring-1 bg-green-600 text-white focus:ring-0"
    : "text-gray-500 hover:bg-gray-500 hover:text-white";

  return (
    <button
      onClick={() => onClickHandler(label)}
      className={`px-4 py-1 mx-2 mt-2 text-sm font-medium transition-colors duration-200 transform
        rounded-full md:mt-0 focus:outline-none
        ${cssClasses}`}
    >
      {t(`faqs.${label}TabLabel`)}
    </button>
  );
};

export default function Faqs() {
  const { t, lang } = useTranslation("home");
  const [selectedTab, setSelectedTab] = useState("general");

  const Component = (props) => (
    <h3
      {...props}
      className="text-2xl font-medium text-gray-500 uppercase md:text-4xl"
    />
  );

  return (
    <section className="flex flex-col items-center pb-24 bg-waves-background bg-cover bg-right-top">
      <div className="text-center mt-12">
        <Trans
          i18nKey="home:faqs.title"
          components={[<Component />, <span className="text-gray-700" />]}
        />

        <p className="pt-8 pb-12 text-gray-600">{t("faqs.subtitle")}</p>
      </div>

      <Fade triggerOnce cascade>
        <nav>
          <div className="container px-6 py-4 mx-auto">
            <div className="md:flex md:items-center md:justify-between">
              <div className="flex-1 md:flex md:items-center md:justify-between">
                <div className="flex flex-col -mx-4 md:flex-row md:items-center md:mx-8">
                  {Object.keys(FAQS).map((label) => (
                    <Button
                      key={label}
                      label={label}
                      active={label === selectedTab}
                      onClickHandler={setSelectedTab}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </nav>

        <div>
          {selectedTab === "general" && <GeneralFaqs />}
          {selectedTab === "fetchAi" && <FetchAiFaqs />}
          {selectedTab === "cartesi" && <CartesiFaqs />}
        </div>
      </Fade>
    </section>
  );
}
