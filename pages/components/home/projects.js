import { useState } from "react";
import Link from "next/link";
import { Fade } from "react-awesome-reveal";
import useTranslation from "next-translate/useTranslation";
import Trans from "next-translate/Trans";

import styles from "./projects.module.css";

const PROJECTS = ["fetchAi", "cartesi", "video", "comingSoon"];

const renderProjectBullets = (selectedTab, setSelectedTab) => (
  <div className="flex justify-center order-2 mt-6 md:mt-0 md:space-y-3 md:flex-col mr-12 lg:mr-24">
    {PROJECTS.map((project, i) => {
      const cssClases =
        i === selectedTab ? "bg-pink-500" : "bg-gray-300 hover:bg-pink-500";

      return (
        <button
          key={project}
          className={`w-3 h-3 mx-2 bg-pink-500 rounded-full md:mx-0 focus:outline-none ${cssClases}`}
          onClick={() => setSelectedTab(i)}
        ></button>
      );
    })}
  </div>
);

const renderFetchAiProjectCard = (t, selectedTab, setSelectedTab) => {
  const Component = (props) => (
    <span {...props} className="text-gray-500 font-semibold" />
  );

  return (
    <div className="container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6">
      <div className="flex flex-col items-center w-full md:flex-row md:w-1/2">
        {renderProjectBullets(selectedTab, setSelectedTab)}

        <div className="md:order-2 mb-8 md:mb-0">
          <h1 className="text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl">
            {t("projects.fetchAiServiceTitle")}
          </h1>
          <p className="mt-6 text-gray-600 dark:text-gray-300">
            {t("projects.fetchAiServiceSubtitle")}
          </p>
          <div className="mt-8 text-lg">
            <Trans
              i18nKey="home:projects.fetchAiServiceOffer1"
              components={[<Component />, <span className="text-pink-700" />]}
            />
          </div>
        </div>
      </div>

      <div className="flex flex-col items-center justify-center w-full h-96 md:w-1/2  bg-hexagons-network-background bg-center bg-cover">
        <img
          src="/img/fetch/fetch_robot_no_background.png"
          width={"160px"}
          height={"200px"}
          className="animate-floatingY"
        />
        <div className="mt-8">
          <Link href="https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23">
            <a
              target="_blank"
              className="px-3 py-2 mt-12 text-sm font-medium uppercase rounded-md text-gray-600 hover:text-pink-600 focus:outline-none hover:border-pink-500 border-4 border-gray-400"
            >
              {t("projects.fetchAiServiceButton")}
            </a>
          </Link>
        </div>
      </div>
    </div>
  );
};

const renderCartesiProjectCard = (
  t,
  selectedTab,
  setSelectedTab,
  cartesiTooltipVisible,
  setCartesiTooltipVisible
) => {
  const tooltipCssClass = cartesiTooltipVisible ? "block" : "hidden";

  return (
    <div className="container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6">
      <div className="flex flex-col items-center w-full md:flex-row md:w-1/2">
        {renderProjectBullets(selectedTab, setSelectedTab)}

        <div className="md:order-2 mb-8 md:mb-0">
          <h1 className="text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl">
            {t("projects.cartesiServiceTitle")}
          </h1>
          <p className="mt-6 text-gray-600 dark:text-gray-300">
            {t("projects.cartesiServiceSubtitle")}
          </p>
          <div className="mt-8 text-lg">
            <span className="text-gray-500 font-semibold">
              {t("projects.cartesiServiceOffer1")}
            </span>
          </div>
        </div>
      </div>

      <div className="flex flex-col items-center justify-center w-full h-96 md:w-1/2 bg-building-blocks-background bg-no-repeat bg-center">
        <img
          src="/img/cartesi/logo_cartesi.png"
          className="rounded-full h-36 w-36"
        />

        <div className="mb-6 mt-12">
          <a
            className="px-3 py-2 mt-12 text-sm font-medium uppercase rounded-md text-gray-600 hover:text-pink-600 focus:outline-none hover:border-pink-500 border-4 border-gray-400 cursor-pointer"
            onMouseOver={() => setCartesiTooltipVisible(true)}
            onMouseOut={() => setCartesiTooltipVisible(false)}
          >
            {t("projects.cartesiServiceButton")}
          </a>

          <div class={`relative mx-2 -top-10 left-16 ${tooltipCssClass}`}>
            <div class="absolute bg-black text-white text-xs rounded py-2 px-4 right-0 bottom-full">
              {t("projects.comingSoonTabLabel")}
              <svg
                class="absolute text-black h-4 left-0 ml-3 top-full"
                x="0px"
                y="0px"
                viewBox="0 0 255 255"
              >
                <polygon class="fill-current" points="0,0 127.5,127.5 255,0" />
              </svg>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const renderVideoProjectCard = (t, selectedTab, setSelectedTab) => (
  <div className="container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6">
    <div className="flex flex-col items-center w-full md:flex-row md:w-1/2">
      {renderProjectBullets(selectedTab, setSelectedTab)}

      <div className="md:order-2 mb-8 md:mb-0">
        <h1 className="text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl">
          {t("projects.videoServiceTitle")}
        </h1>
        <p className="mt-6 text-gray-600 dark:text-gray-300">
          {t("projects.videoServiceSubtitle")}
        </p>
      </div>
    </div>

    <div className="flex flex-col items-center justify-center w-full h-96 md:w-1/2 bg-circuit-wires-background bg-no-repeat bg-center">
      <div className="flex flew-row spate-between space-x-24 animate-create">
        <img
          src="/img/theta/theta-logo.png"
          className="rounded-full h-36 w-36"
        />
        <img src="/img/aioz/aioz-logo.png" className="rounded-full h-36 w-36" />
      </div>
    </div>
  </div>
);

const renderComingSoonCard = (t, selectedTab, setSelectedTab) => (
  <div className="container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6">
    <div className="flex flex-col items-center w-full md:flex-row md:w-1/2">
      {renderProjectBullets(selectedTab, setSelectedTab)}

      <div className="md:order-2 mb-8 md:mb-0">
        <h1 className="text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl">
          {t("projects.comingSoonServiceTitle")}
        </h1>
        <p className="mt-6 text-gray-600 dark:text-gray-300">
          {t("projects.comingSoonServiceSubtitle")}
        </p>
        <div className="mt-8 text-lg">
          <span className="text-gray-500 font-semibold">
            {t("projects.comingSoonServiceOffer1")}
          </span>
          <span className="text-gray-600">
            {t("projects.comingSoonServiceOffer2")}
          </span>
        </div>
      </div>
    </div>

    <div className="flex flex-col items-center justify-center w-full h-96 md:w-1/2 animate-fadeIn">
      <img
        src="/img/coming-soon/white-coin-question-mark.png"
        width={"200px"}
        height={"200px"}
        className={styles.coinShadow}
      />
    </div>
  </div>
);

const renderProjectCard = (
  t,
  selectedTab,
  setTabSelected,
  cartesiTooltipVisible,
  setCartesiTooltipVisible
) => {
  const projectsRenderers = [
    renderFetchAiProjectCard,
    renderCartesiProjectCard,
    renderVideoProjectCard,
    renderComingSoonCard,
  ];

  return projectsRenderers[selectedTab](
    t,
    selectedTab,
    setTabSelected,
    cartesiTooltipVisible,
    setCartesiTooltipVisible
  );
};

export default function Projects() {
  const { t, lang } = useTranslation("home");
  const [selectedTab, setSelectedTab] = useState(0);
  const [cartesiTooltipVisible, setCartesiTooltipVisible] = useState(false);

  const Component = (props) => (
    <h3
      {...props}
      className="text-2xl font-medium text-gray-500 uppercase md:text-4xl"
    />
  );

  return (
    <Fade triggerOnce cascade>
      <section>
        <div className="container px-6 pt-8 mb-16 mx-auto">
          <div className="text-center mt-16 mb-8">
            <Trans
              i18nKey="home:projects.title"
              components={[<Component />, <span className="text-gray-700" />]}
            />
            <p className="pt-8 pb-8 text-gray-600">{t("projects.subtitle")}</p>
          </div>

          <Fade triggerOnce cascade damping="0.2">
            <nav>
              <div className="container mx-auto mb-8">
                <div className="flex items-center justify-center text-gray-600 capitalize">
                  {PROJECTS.map((project, i) => {
                    const cssClases =
                      i === selectedTab
                        ? "text-gray-800 border-b-2 border-pink-500"
                        : "border-transparent hover:text-gray-800 hover:border-pink-500";

                    return (
                      <a
                        key={project}
                        onClick={() => setSelectedTab(i)}
                        className={`border-b-2 px-1 sm:mx-6 cursor-pointer ${cssClases}`}
                      >
                        {t(`projects.${project}TabLabel`)}
                      </a>
                    );
                  })}
                </div>
              </div>
            </nav>

            {renderProjectCard(
              t,
              selectedTab,
              setSelectedTab,
              cartesiTooltipVisible,
              setCartesiTooltipVisible
            )}
          </Fade>
        </div>
      </section>
    </Fade>
  );
}
