import useTranslation from "next-translate/useTranslation";
import { Fade, Zoom } from "react-awesome-reveal";
import Trans from "next-translate/Trans";

import {
  CurrencyDollarIcon,
  ChartBarIcon,
  ClockIcon,
} from "@heroicons/react/solid";

export default function TimeInvestment() {
  const { t, lang } = useTranslation("home");

  const Component = (props) => (
    <h3
      {...props}
      className="text-2xl font-medium text-gray-500 uppercase md:text-4xl"
    />
  );

  return (
    <Fade triggerOnce>
      <section className="bg-white dark:bg-gray-800 bg-sparse-hexagons-background bg-center bg-cover">
        <div className="container px-6 py-8 pb-16 mx-auto">
          <div className="text-center mt-16 mb-8">
            <Trans
              i18nKey="home:timeInvestment.title"
              components={[<Component />, <span className="text-gray-700" />]}
            />
            <p className="py-8 text-gray-600">{t("timeInvestment.subtitle")}</p>
          </div>
          <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
            <Fade triggerOnce cascade damping="0.2">
              <div className="mx-8 mb-8">
                <CurrencyDollarIcon className="h-10 w-10 text-green-500" />

                <h1 className="my-6 text-xl font-semibold text-gray-800 dark:text-white">
                  {t("timeInvestment.delegateTitle")}
                </h1>

                <p className="mt-2 text-gray-500 dark:text-gray-400">
                  {t("timeInvestment.delegateParagraph")}
                </p>
              </div>
              <div className="mx-8 mb-8">
                <ChartBarIcon className="h-10 w-10 text-green-500" />

                <h1 className="my-6 text-xl font-semibold text-gray-800 dark:text-white">
                  {t("timeInvestment.incomeTitle")}
                </h1>

                <p className="mt-2 text-gray-500 dark:text-gray-400">
                  {t("timeInvestment.incomeParagraph")}
                </p>
              </div>
              <div className="mx-8 mb-8">
                <ClockIcon className="h-10 w-10 text-green-500" />

                <h1 className="my-6 text-xl font-semibold text-gray-800 dark:text-white">
                  {t("timeInvestment.timeTitle")}
                </h1>

                <p className="mt-2 text-gray-500 dark:text-gray-400">
                  {t("timeInvestment.timeParagraph")}
                </p>
              </div>
            </Fade>
          </div>
        </div>
      </section>
    </Fade>
  );
}
