import Particles from "react-tsparticles";
import { Fade, Zoom } from "react-awesome-reveal";
import useTranslation from "next-translate/useTranslation";

import styles from "./technology.module.css";

const particlesInit = () => {};
const particlesLoaded = () => {};

const particlesOptions = {
  autoPlay: true,
  particles: {
    number: { value: 90, density: { enable: true, value_area: 800 } },
    color: {
      value: "#9e1068",
      animation: {
        h: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false,
        },
        s: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false,
        },
        l: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false,
        },
      },
    },
    opacity: {
      value: 0.3,
      random: true,
      anim: { enable: true, speed: 10, opacity_min: 0.1, sync: false },
    },
    size: {
      value: 4,
      random: true,
      anim: { enable: false, speed: 2, size_min: 0.1, sync: false },
    },
    line_linked: {
      enable: true,
      distance: 179,
      color: "#c41d7f",
      opacity: 0.5,
      width: 1,
    },
    move: {
      enable: true,
      speed: 0.5,
      direction: "none",
      random: false,
      straight: false,
      out_mode: "out",
      bounce: false,
      attract: { enable: false, rotateX: 600, rotateY: 1200 },
    },
  },
  retina_detect: true,
};

export default function technology() {
  const { t, lang } = useTranslation("home");

  return (
    <section className={`relative ${styles.outerSpace}`}>
      <div className="text-center absolute top-0 inset-0 mt-12">
        <h3 className="text-2xl font-medium text-gray-400 uppercase md:text-4xl">
          {t("technology.title")}

          <div className="pt-6 px-6 flex flex-col items-center">
            <img
              src="/img/logos/blockscope_logo_only-letters_white.svg"
              width={"382px"}
              height={"37px"}
            />
          </div>
        </h3>
        <p className="mt-12 text-xl text-gray-300">
          {t("technology.subtitle")}
        </p>
      </div>

      <div id="particles-js" className="mx-auto md:h-128 h-192">
        <Particles
          id="tsparticlesStars"
          init={particlesInit}
          loaded={particlesLoaded}
          options={particlesOptions}
          className={styles.canvasNetwork}
        />
      </div>

      <div className="container px-6 mx-auto absolute top-72 inset-0">
        <div className="w-full flex flex-col items-center mt-2">
          <div className="flex flex-col space-y-16 lg:space-y-0 lg:space-x-16 lg:flex-row lg:content-around">
            <Zoom triggerOnce cascade damping="0.2" duration="600">
              <div
                className={`max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${styles.logoShadow}`}
              >
                <div>
                  <div className="flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-pink-500 via-red-500 to-pink-700">
                    <img
                      src="/img/logos/blockscope_logo_no-letters_white_solid_200.png"
                      width="40px"
                      height="44px"
                    />
                  </div>
                  <Fade triggerOnce cascade damping="0.2" delay={200}>
                    <p className="font-bold text-center text-xl text-gray-500 mt-6">
                      {t("technology.performanceCardTitle")}
                    </p>
                    <p className="xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8">
                      {t("technology.performanceCardParagraph")}
                    </p>
                  </Fade>
                </div>
              </div>

              <div
                className={`max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${styles.logoShadowGreen}`}
              >
                <div>
                  <div className="flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-green-400 via-green-600 bg-green-700">
                    <img
                      src="/img/logos/blockscope_logo_no-letters_white_solid_200.png"
                      width="40px"
                      height="44px"
                    />
                  </div>
                  <Fade triggerOnce cascade damping="0.2" delay={200}>
                    <p className="font-bold text-center text-xl text-gray-500 mt-6">
                      {t("technology.securityCardTitle")}
                    </p>
                    <p className="xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8">
                      {t("technology.securityCardParagraph")}
                    </p>
                  </Fade>
                </div>
              </div>

              <div
                className={`max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${styles.logoShadow}`}
              >
                <div>
                  <div className="flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-pink-500 via-red-500 to-pink-700">
                    <img
                      src="/img/logos/blockscope_logo_no-letters_white_solid_200.png"
                      width="40px"
                      height="44px"
                    />
                  </div>
                  <Fade triggerOnce cascade damping="0.2" delay={200}>
                    <p className="font-bold text-center text-xl text-gray-500 mt-6">
                      {t("technology.experienceCardTitle")}
                    </p>
                    <p className="xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8">
                      {t("technology.experienceCardParagraph")}
                    </p>
                  </Fade>
                </div>
              </div>
            </Zoom>
          </div>
        </div>
      </div>
    </section>
  );
}
