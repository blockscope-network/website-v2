import Link from "next/link";
import Particles from "react-tsparticles";
import useTranslation from "next-translate/useTranslation";

import styles from "./hero.module.css";

const particlesInit = () => {};
const particlesLoaded = () => {};

const particlesOptions = {
  autoPlay: true,
  detectRetina: true,
  fpsLimit: 60,
  infection: {
    enable: false,
  },
  manualParticles: [],
  motion: {
    disable: false,
    reduce: {
      factor: 4,
      value: true,
    },
  },
  particles: {
    bounce: {
      horizontal: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
      vertical: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
    },
    collisions: {
      bounce: {
        horizontal: {
          random: {
            enable: false,
            minimumValue: 0.1,
          },
          value: 1,
        },
        vertical: {
          random: {
            enable: false,
            minimumValue: 0.1,
          },
          value: 1,
        },
      },
      enable: false,
      mode: "bounce",
      overlap: {
        enable: true,
        retries: 0,
      },
    },
    color: {
      value: "#9e1068",
      animation: {
        h: {
          count: 1,
          enable: false,
          offset: 0,
          speed: 100,
          sync: false,
        },
        s: {
          count: 0,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false,
        },
        l: {
          count: 0,
          enable: false,
          offset: 0,
          speed: 1,
          sync: false,
        },
      },
    },
    destroy: {
      mode: "none",
    },
    life: {
      count: 0,
      delay: {
        random: {
          enable: false,
          minimumValue: 0,
        },
        value: 0,
        sync: false,
      },
      duration: {
        random: {
          enable: false,
          minimumValue: 0.0001,
        },
        value: 0,
        sync: false,
      },
    },
    links: {
      blink: false,
      enable: false,
    },
    move: {
      angle: {
        offset: 45,
        value: 90,
      },
      attract: {
        enable: true,
        rotate: {
          x: 600,
          y: 1200,
        },
      },
      decay: 0,
      distance: 0,
      direction: "none",
      drift: 0,
      enable: true,
      gravity: {
        acceleration: 9.81,
        enable: false,
        maxSpeed: 50,
      },
      path: {
        clamp: true,
        delay: {
          random: {
            enable: false,
            minimumValue: 0,
          },
          value: 0,
        },
        enable: false,
      },
      outModes: {
        default: "out",
        bottom: "out",
        left: "out",
        right: "out",
        top: "out",
      },
      random: false,
      size: false,
      speed: 2,
      straight: false,
      trail: {
        enable: false,
        length: 10,
        fillColor: {
          value: "#000000",
        },
      },
      vibrate: false,
      warp: false,
    },
    number: {
      density: {
        enable: true,
        area: 800,
        factor: 1000,
      },
      limit: 0,
      value: 12,
    },
    opacity: {
      random: {
        enable: true,
        minimumValue: 0.3,
      },
      value: {
        min: 0.3,
        max: 0.5,
      },
      animation: {
        count: 0,
        enable: false,
        speed: 1,
        sync: false,
        destroy: "none",
        minimumValue: 0.1,
        startValue: "random",
      },
    },
    reduceDuplicates: false,
    rotate: {
      random: {
        enable: false,
        minimumValue: 30,
      },
      value: 360,
      animation: {
        enable: true,
        speed: 5,
        sync: false,
      },
      direction: "clockwise",
      path: true,
    },
    shadow: {
      blur: 20,
      color: {
        value: "#111111",
      },
      enable: true,
      offset: {
        x: 5,
        y: 5,
      },
    },
    shape: {
      options: {
        polygon: {
          sides: 6,
        },
        star: {
          sides: 6,
        },
      },
      type: "polygon",
    },
    size: {
      random: {
        enable: true,
        minimumValue: 10,
      },
      value: {
        min: 10,
        max: 80,
      },
      animation: {
        count: 0,
        enable: true,
        speed: 10,
        sync: false,
        destroy: "none",
        minimumValue: 100,
        startValue: "random",
      },
    },
    stroke: {
      width: 0,
    },
    twinkle: {
      lines: {
        enable: false,
      },
      particles: {
        enable: false,
      },
    },
  },
  pauseOnBlur: false,
  pauseOnOutsideViewport: false,
  responsive: [],
  themes: [],
};

export default function Hero() {
  const { t, lang } = useTranslation("home");

  return (
    <div
      id="particles-js"
      className="mx-auto mt-24 bg-black"
      style={{ height: "630px" }}
    >
      <Particles
        id="tsparticles"
        init={particlesInit}
        loaded={particlesLoaded}
        options={particlesOptions}
        height={"630px"}
      />

      <div
        className={`items-center relative xl:px-40 md:flex flex-col xl:flex-row ${styles.logo}`}
      >
        <div className="w-full flex flex-col items-center hidden xl:block">
          <div className="max-w-lg w-5/6">
            <h1 className="text-2xl font-semibold text-gray-200 uppercase md:text-4xl">
              <span className="text-pink-600">
                {t("hero.stakingServicesTitle")}
              </span>
              <br />
              {t("hero.forTitle")}{" "}
              <span className="text-gray-400">{t("hero.cryptoTitle")}</span>{" "}
              {t("hero.investorsTitle")}
            </h1>

            <p className="mt-8 text-gray-400">
              {t("hero.delegateInvestmentsSubtitle")}
            </p>

            <div className="w-full flex flex-row items-center space-x-8">
              <Link href="https://t.me/joinchat/12BDqxGu01kyYjU0">
                <a
                  target="_blank"
                  className="px-3 py-3 mt-12 text-sm font-medium text-white uppercase bg-pink-600 rounded-md hover:bg-pink-500 focus:outline-none focus:bg-pink-500"
                >
                  {t("hero.joinTelegramButton")}
                </a>
              </Link>

              <Link href="https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23">
                <a
                  target="_blank"
                  className="px-3 py-2 mt-12 text-sm font-medium text-white uppercase rounded-md hover:text-pink-500 focus:outline-none focus:bg-pink-500 hover:border-pink-500 border-4"
                >
                  {t("hero.fetchAiStakingButton")}
                </a>
              </Link>
            </div>
          </div>
        </div>

        <div className="flex flex-col items-center justify-center w-full">
          <h1 className="text-2xl font-semibold text-gray-200 text-center uppercase md:text-4xl xl:hidden mb-8">
            <span className="text-pink-600">
              {t("hero.stakingServicesTitle")}
            </span>
            <br />
            {t("hero.forTitle")}{" "}
            <span className="text-gray-400">{t("hero.cryptoTitle")}</span>{" "}
            {t("hero.investorsTitle")}
          </h1>

          <div className="xl:w-2/3 w-64 text-center">
            <img
              src="/img/logos/blockscope_logo_no-letters_black_white_border-white_200.svg"
              alt="blockscope universe backgound"
              width="400px"
              height="400px"
            />
          </div>

          <div className="flex flex-row space-x-8 xl:hidden text-center">
            <Link href="https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23">
              <a
                target="_blank"
                className="px-3 py-3 mt-12 text-sm font-medium text-white uppercase bg-pink-600 rounded-md hover:bg-pink-500 focus:outline-none focus:bg-pink-500"
              >
                {t("hero.joinTelegramButton")}
              </a>
            </Link>
            <Link href="https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23">
              <a
                target="_blank"
                className="px-3 py-2 mt-12 text-sm font-medium text-white uppercase rounded-md hover:text-pink-500 focus:outline-none focus:bg-pink-500 hover:border-pink-500 border-4"
              >
                {t("hero.fetchAiStakingButton")}
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
