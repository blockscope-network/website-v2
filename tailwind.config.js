module.exports = {
  mode: "jit",
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    opacity: {
      0: "0",
      10: ".1",
      20: ".2",
      30: ".3",
      40: ".4",
      50: ".5",
      60: ".6",
      70: ".7",
      80: ".8",
      90: ".9",
      100: "1",
    },
    extend: {
      colors: {
        pink: {
          100: "#fff0f6",
          200: "#ffadd2",
          300: "#ff85c0",
          400: "#f759ab",
          500: "#eb2f96",
          600: "#c41d7f",
          700: "#9e1068",
          800: "#780650",
          900: "#520339",
        },

        green: {
          100: "#f6ffed",
          200: "#b7eb8f",
          300: "#95de64",
          400: "#73d13d",
          500: "#52c41a",
          600: "#389e0d",
          700: "#237804",
          800: "#135200",
          900: "#092b00",
        },
      },
      animation: {
        floatingY: "floatAnimation 5s ease-in-out infinite",
        grow: "growAnimation 10s ease-in-out infinite",
        create: "createAnimation 0.3s ease-in-out 1",
        fadeIn: "fadeInAnimation 0.3s ease-in-out 1",
        fadeInFast: "fadeInAnimation 0.1s ease-in-out 1",
        fadeOutFast: "fadeInAnimation 0.1s ease-in-out reverse 1",
      },
      keyframes: {
        floatAnimation: {
          "0%": { transform: "translate(0, 0px)" },
          "65%": { transform: "translate(0, 10px)" },
          "100%": { transform: "translate(0, -0px)" },
        },
        growAnimation: {
          "0%": { transform: "scale(1)" },
          "65%": { transform: "scale(0.96)" },
          "100%": { transform: "scale(1)" },
        },
        createAnimation: {
          "0%": { transform: "scale(0.1)" },
          "100%": { transform: "scale(1)" },
        },
        fadeInAnimation: {
          "0%": { opacity: "0" },
          "100%": { opacity: "1" },
        },
      },
      backgroundImage: (theme) => ({
        "hexagon-background":
          "url('/img/backgrounds/hexagon_background_dark.jpg')",
        "sparse-hexagons-background":
          "url('/img/backgrounds/blockscope_sparse_hexagons_bg2.png')",
        "hexagons-network-background":
          "url('/img/backgrounds/blockscope_hexagons_network_bg.png')",
        "building-blocks-background":
          "url('/img/backgrounds/cubos_cartesi_small_clear.svg')",
        "question-hexagons-background":
          "url('/img/backgrounds/technology-token-question-background.jpg')",
        "circuit-wires-background":
          "url('/img/backgrounds/circuit-wires-background.png')",
        "waves-background": "url('/img/backgrounds/cartesi-mesh.svg')",
      }),
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
