exports.id = 45;
exports.ids = [45];
exports.modules = {

/***/ 45:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Projects; }
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1664);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(104);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(8841);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var next_translate_Trans__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(2464);
/* harmony import */ var next_translate_Trans__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_translate_Trans__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _projects_module_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(278);
/* harmony import */ var _projects_module_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_projects_module_css__WEBPACK_IMPORTED_MODULE_6__);



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const PROJECTS = ["fetchAi", "cartesi", "video", "comingSoon"];

const renderProjectBullets = (selectedTab, setSelectedTab) => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
  className: "flex justify-center order-2 mt-6 md:mt-0 md:space-y-3 md:flex-col mr-12 lg:mr-24",
  children: PROJECTS.map((project, i) => {
    const cssClases = i === selectedTab ? "bg-pink-500" : "bg-gray-300 hover:bg-pink-500";
    return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
      className: `w-3 h-3 mx-2 bg-pink-500 rounded-full md:mx-0 focus:outline-none ${cssClases}`,
      onClick: () => setSelectedTab(i)
    }, project);
  })
});

const renderFetchAiProjectCard = (t, selectedTab, setSelectedTab) => {
  const Component = props => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", _objectSpread(_objectSpread({}, props), {}, {
    className: "text-gray-500 font-semibold"
  }));

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col items-center w-full md:flex-row md:w-1/2",
      children: [renderProjectBullets(selectedTab, setSelectedTab), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "md:order-2 mb-8 md:mb-0",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
          className: "text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl",
          children: t("projects.fetchAiServiceTitle")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "mt-6 text-gray-600 dark:text-gray-300",
          children: t("projects.fetchAiServiceSubtitle")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "mt-8 text-lg",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_translate_Trans__WEBPACK_IMPORTED_MODULE_5___default()), {
            i18nKey: "home:projects.fetchAiServiceOffer1",
            components: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
              className: "text-pink-700"
            })]
          })
        })]
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col items-center justify-center w-full h-96 md:w-1/2  bg-hexagons-network-background bg-center bg-cover",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
        src: "/img/fetch/fetch_robot_no_background.png",
        width: "160px",
        height: "200px",
        className: "animate-floatingY"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "mt-8",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_2__.default, {
          href: "https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
            target: "_blank",
            className: "px-3 py-2 mt-12 text-sm font-medium uppercase rounded-md text-gray-600 hover:text-pink-600 focus:outline-none hover:border-pink-500 border-4 border-gray-400",
            children: t("projects.fetchAiServiceButton")
          })
        })
      })]
    })]
  });
};

const renderCartesiProjectCard = (t, selectedTab, setSelectedTab, cartesiTooltipVisible, setCartesiTooltipVisible) => {
  const tooltipCssClass = cartesiTooltipVisible ? "block" : "hidden";
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col items-center w-full md:flex-row md:w-1/2",
      children: [renderProjectBullets(selectedTab, setSelectedTab), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "md:order-2 mb-8 md:mb-0",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
          className: "text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl",
          children: t("projects.cartesiServiceTitle")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
          className: "mt-6 text-gray-600 dark:text-gray-300",
          children: t("projects.cartesiServiceSubtitle")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "mt-8 text-lg",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
            className: "text-gray-500 font-semibold",
            children: t("projects.cartesiServiceOffer1")
          })
        })]
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-col items-center justify-center w-full h-96 md:w-1/2 bg-building-blocks-background bg-no-repeat bg-center",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
        src: "/img/cartesi/logo_cartesi.png",
        className: "rounded-full h-36 w-36"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "mb-6 mt-12",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
          className: "px-3 py-2 mt-12 text-sm font-medium uppercase rounded-md text-gray-600 hover:text-pink-600 focus:outline-none hover:border-pink-500 border-4 border-gray-400 cursor-pointer",
          onMouseOver: () => setCartesiTooltipVisible(true),
          onMouseOut: () => setCartesiTooltipVisible(false),
          children: t("projects.cartesiServiceButton")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          class: `relative mx-2 -top-10 left-16 ${tooltipCssClass}`,
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            class: "absolute bg-black text-white text-xs rounded py-2 px-4 right-0 bottom-full",
            children: [t("projects.comingSoonTabLabel"), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("svg", {
              class: "absolute text-black h-4 left-0 ml-3 top-full",
              x: "0px",
              y: "0px",
              viewBox: "0 0 255 255",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("polygon", {
                class: "fill-current",
                points: "0,0 127.5,127.5 255,0"
              })
            })]
          })
        })]
      })]
    })]
  });
};

const renderVideoProjectCard = (t, selectedTab, setSelectedTab) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
  className: "container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6",
  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex flex-col items-center w-full md:flex-row md:w-1/2",
    children: [renderProjectBullets(selectedTab, setSelectedTab), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "md:order-2 mb-8 md:mb-0",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
        className: "text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl",
        children: t("projects.videoServiceTitle")
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "mt-6 text-gray-600 dark:text-gray-300",
        children: t("projects.videoServiceSubtitle")
      })]
    })]
  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    className: "flex flex-col items-center justify-center w-full h-96 md:w-1/2 bg-circuit-wires-background bg-no-repeat bg-center",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flew-row spate-between space-x-24 animate-create",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
        src: "/img/theta/theta-logo.png",
        className: "rounded-full h-36 w-36"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
        src: "/img/aioz/aioz-logo.png",
        className: "rounded-full h-36 w-36"
      })]
    })
  })]
});

const renderComingSoonCard = (t, selectedTab, setSelectedTab) => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
  className: "container flex flex-col px-6 py-4 mx-auto md:h-128 md:py-6 md:flex-row md:items-center md:space-x-6",
  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex flex-col items-center w-full md:flex-row md:w-1/2",
    children: [renderProjectBullets(selectedTab, setSelectedTab), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "md:order-2 mb-8 md:mb-0",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h1", {
        className: "text-2xl font-medium tracking-wide text-gray-600 dark:text-white md:text-3xl",
        children: t("projects.comingSoonServiceTitle")
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "mt-6 text-gray-600 dark:text-gray-300",
        children: t("projects.comingSoonServiceSubtitle")
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "mt-8 text-lg",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
          className: "text-gray-500 font-semibold",
          children: t("projects.comingSoonServiceOffer1")
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
          className: "text-gray-600",
          children: t("projects.comingSoonServiceOffer2")
        })]
      })]
    })]
  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
    className: "flex flex-col items-center justify-center w-full h-96 md:w-1/2 animate-fadeIn",
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
      src: "/img/coming-soon/white-coin-question-mark.png",
      width: "200px",
      height: "200px",
      className: (_projects_module_css__WEBPACK_IMPORTED_MODULE_6___default().coinShadow)
    })
  })]
});

const renderProjectCard = (t, selectedTab, setTabSelected, cartesiTooltipVisible, setCartesiTooltipVisible) => {
  const projectsRenderers = [renderFetchAiProjectCard, renderCartesiProjectCard, renderVideoProjectCard, renderComingSoonCard];
  return projectsRenderers[selectedTab](t, selectedTab, setTabSelected, cartesiTooltipVisible, setCartesiTooltipVisible);
};

function Projects() {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_4___default()("home");
  const {
    0: selectedTab,
    1: setSelectedTab
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);
  const {
    0: cartesiTooltipVisible,
    1: setCartesiTooltipVisible
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(false);

  const Component = props => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", _objectSpread(_objectSpread({}, props), {}, {
    className: "text-2xl font-medium text-gray-500 uppercase md:text-4xl"
  }));

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_3__.Fade, {
    triggerOnce: true,
    cascade: true,
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("section", {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "container px-6 pt-8 mb-16 mx-auto",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "text-center mt-16 mb-8",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_translate_Trans__WEBPACK_IMPORTED_MODULE_5___default()), {
            i18nKey: "home:projects.title",
            components: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
              className: "text-gray-700"
            })]
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "pt-8 pb-8 text-gray-600",
            children: t("projects.subtitle")
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_3__.Fade, {
          triggerOnce: true,
          cascade: true,
          damping: "0.2",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "container mx-auto mb-8",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "flex items-center justify-center text-gray-600 capitalize",
                children: PROJECTS.map((project, i) => {
                  const cssClases = i === selectedTab ? "text-gray-800 border-b-2 border-pink-500" : "border-transparent hover:text-gray-800 hover:border-pink-500";
                  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                    onClick: () => setSelectedTab(i),
                    className: `border-b-2 px-1 sm:mx-6 cursor-pointer ${cssClases}`,
                    children: t(`projects.${project}TabLabel`)
                  }, project);
                })
              })
            })
          }), renderProjectCard(t, selectedTab, setSelectedTab, cartesiTooltipVisible, setCartesiTooltipVisible)]
        })]
      })
    })
  });
}

/***/ }),

/***/ 278:
/***/ (function(module) {

// Exports
module.exports = {
	"coinShadow": "projects_coinShadow__2CNkC"
};


/***/ })

};
;