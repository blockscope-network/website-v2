exports.id = 622;
exports.ids = [622];
exports.modules = {

/***/ 622:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Hero; }
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1664);
/* harmony import */ var react_tsparticles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6810);
/* harmony import */ var react_tsparticles__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_tsparticles__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8841);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _hero_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(4793);
/* harmony import */ var _hero_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_hero_module_css__WEBPACK_IMPORTED_MODULE_4__);







const particlesInit = () => {};

const particlesLoaded = () => {};

const particlesOptions = {
  autoPlay: true,
  detectRetina: true,
  fpsLimit: 60,
  infection: {
    enable: false
  },
  manualParticles: [],
  motion: {
    disable: false,
    reduce: {
      factor: 4,
      value: true
    }
  },
  particles: {
    bounce: {
      horizontal: {
        random: {
          enable: false,
          minimumValue: 0.1
        },
        value: 1
      },
      vertical: {
        random: {
          enable: false,
          minimumValue: 0.1
        },
        value: 1
      }
    },
    collisions: {
      bounce: {
        horizontal: {
          random: {
            enable: false,
            minimumValue: 0.1
          },
          value: 1
        },
        vertical: {
          random: {
            enable: false,
            minimumValue: 0.1
          },
          value: 1
        }
      },
      enable: false,
      mode: "bounce",
      overlap: {
        enable: true,
        retries: 0
      }
    },
    color: {
      value: "#9e1068",
      animation: {
        h: {
          count: 1,
          enable: false,
          offset: 0,
          speed: 100,
          sync: false
        },
        s: {
          count: 0,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false
        },
        l: {
          count: 0,
          enable: false,
          offset: 0,
          speed: 1,
          sync: false
        }
      }
    },
    destroy: {
      mode: "none"
    },
    life: {
      count: 0,
      delay: {
        random: {
          enable: false,
          minimumValue: 0
        },
        value: 0,
        sync: false
      },
      duration: {
        random: {
          enable: false,
          minimumValue: 0.0001
        },
        value: 0,
        sync: false
      }
    },
    links: {
      blink: false,
      enable: false
    },
    move: {
      angle: {
        offset: 45,
        value: 90
      },
      attract: {
        enable: true,
        rotate: {
          x: 600,
          y: 1200
        }
      },
      decay: 0,
      distance: 0,
      direction: "none",
      drift: 0,
      enable: true,
      gravity: {
        acceleration: 9.81,
        enable: false,
        maxSpeed: 50
      },
      path: {
        clamp: true,
        delay: {
          random: {
            enable: false,
            minimumValue: 0
          },
          value: 0
        },
        enable: false
      },
      outModes: {
        default: "out",
        bottom: "out",
        left: "out",
        right: "out",
        top: "out"
      },
      random: false,
      size: false,
      speed: 2,
      straight: false,
      trail: {
        enable: false,
        length: 10,
        fillColor: {
          value: "#000000"
        }
      },
      vibrate: false,
      warp: false
    },
    number: {
      density: {
        enable: true,
        area: 800,
        factor: 1000
      },
      limit: 0,
      value: 12
    },
    opacity: {
      random: {
        enable: true,
        minimumValue: 0.3
      },
      value: {
        min: 0.3,
        max: 0.5
      },
      animation: {
        count: 0,
        enable: false,
        speed: 1,
        sync: false,
        destroy: "none",
        minimumValue: 0.1,
        startValue: "random"
      }
    },
    reduceDuplicates: false,
    rotate: {
      random: {
        enable: false,
        minimumValue: 30
      },
      value: 360,
      animation: {
        enable: true,
        speed: 5,
        sync: false
      },
      direction: "clockwise",
      path: true
    },
    shadow: {
      blur: 20,
      color: {
        value: "#111111"
      },
      enable: true,
      offset: {
        x: 5,
        y: 5
      }
    },
    shape: {
      options: {
        polygon: {
          sides: 6
        },
        star: {
          sides: 6
        }
      },
      type: "polygon"
    },
    size: {
      random: {
        enable: true,
        minimumValue: 10
      },
      value: {
        min: 10,
        max: 80
      },
      animation: {
        count: 0,
        enable: true,
        speed: 10,
        sync: false,
        destroy: "none",
        minimumValue: 100,
        startValue: "random"
      }
    },
    stroke: {
      width: 0
    },
    twinkle: {
      lines: {
        enable: false
      },
      particles: {
        enable: false
      }
    }
  },
  pauseOnBlur: false,
  pauseOnOutsideViewport: false,
  responsive: [],
  themes: []
};
function Hero() {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default()("home");
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    id: "particles-js",
    className: "mx-auto mt-24 bg-black",
    style: {
      height: "630px"
    },
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_tsparticles__WEBPACK_IMPORTED_MODULE_2___default()), {
      id: "tsparticles",
      init: particlesInit,
      loaded: particlesLoaded,
      options: particlesOptions,
      height: "630px"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: `items-center relative xl:px-40 md:flex flex-col xl:flex-row ${(_hero_module_css__WEBPACK_IMPORTED_MODULE_4___default().logo)}`,
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "w-full flex flex-col items-center hidden xl:block",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "max-w-lg w-5/6",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h1", {
            className: "text-2xl font-semibold text-gray-200 uppercase md:text-4xl",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
              className: "text-pink-600",
              children: t("hero.stakingServicesTitle")
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}), t("hero.forTitle"), " ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
              className: "text-gray-400",
              children: t("hero.cryptoTitle")
            }), " ", t("hero.investorsTitle")]
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
            className: "mt-8 text-gray-400",
            children: t("hero.delegateInvestmentsSubtitle")
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
            className: "w-full flex flex-row items-center space-x-8",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "https://t.me/joinchat/12BDqxGu01kyYjU0",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                target: "_blank",
                className: "px-3 py-3 mt-12 text-sm font-medium text-white uppercase bg-pink-600 rounded-md hover:bg-pink-500 focus:outline-none focus:bg-pink-500",
                children: t("hero.joinTelegramButton")
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
              href: "https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
                target: "_blank",
                className: "px-3 py-2 mt-12 text-sm font-medium text-white uppercase rounded-md hover:text-pink-500 focus:outline-none focus:bg-pink-500 hover:border-pink-500 border-4",
                children: t("hero.fetchAiStakingButton")
              })
            })]
          })]
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex flex-col items-center justify-center w-full",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h1", {
          className: "text-2xl font-semibold text-gray-200 text-center uppercase md:text-4xl xl:hidden mb-8",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
            className: "text-pink-600",
            children: t("hero.stakingServicesTitle")
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}), t("hero.forTitle"), " ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
            className: "text-gray-400",
            children: t("hero.cryptoTitle")
          }), " ", t("hero.investorsTitle")]
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "xl:w-2/3 w-64 text-center",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
            src: "/img/logos/blockscope_logo_no-letters_black_white_border-white_200.svg",
            alt: "blockscope universe backgound",
            width: "400px",
            height: "400px"
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
          className: "flex flex-row space-x-8 xl:hidden text-center",
          children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
            href: "https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
              target: "_blank",
              className: "px-3 py-3 mt-12 text-sm font-medium text-white uppercase bg-pink-600 rounded-md hover:bg-pink-500 focus:outline-none focus:bg-pink-500",
              children: t("hero.joinTelegramButton")
            })
          }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(next_link__WEBPACK_IMPORTED_MODULE_1__.default, {
            href: "https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
              target: "_blank",
              className: "px-3 py-2 mt-12 text-sm font-medium text-white uppercase rounded-md hover:text-pink-500 focus:outline-none focus:bg-pink-500 hover:border-pink-500 border-4",
              children: t("hero.fetchAiStakingButton")
            })
          })]
        })]
      })]
    })]
  });
}

/***/ }),

/***/ 4793:
/***/ (function(module) {

// Exports
module.exports = {
	"logo": "hero_logo__1fwXQ"
};


/***/ })

};
;