exports.id = 901;
exports.ids = [901];
exports.modules = {

/***/ 4901:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ Faqs; }
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(9297);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(104);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8841);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_translate_Trans__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2464);
/* harmony import */ var next_translate_Trans__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_translate_Trans__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(3802);



function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







const FaqCard = ({
  question,
  answer,
  htmlInterpolations = []
}) => {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default()("home");
  const {
    0: collapsed,
    1: setCollapsed
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(true);
  const collapsedCssClss = collapsed ? "hidden" : "";

  const Component = props => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Fade, {
    triggerOnce: true,
    duration: 400,
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", _objectSpread(_objectSpread({}, props), {}, {
      className: `mt-4 text-gray-500 text-justify text-sm md:text-base ${collapsedCssClss}`
    }))
  });

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "max-w-md px-8 py-8 mx-auto mt-8 bg-white rounded-lg shadow-lg  text-gray-600 cursor-pointer select-none w-full",
    onClick: () => setCollapsed(!collapsed),
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "flex flex-row space-between content-center",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h2", {
        className: "my-2 mr-4 text-base font-semibold text-gray-500 hover:text-green-600 md:my-0 md:text-base flex-auto",
        children: t("faqs." + question)
      }), collapsed ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_5__/* .PlusSmIcon */ .d2U, {
        className: "h-6 mt-1"
      }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_5__/* .MinusSmIcon */ .Jll, {
        className: "h-6 mt-1"
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_translate_Trans__WEBPACK_IMPORTED_MODULE_4___default()), {
      i18nKey: `home:faqs.${answer}`,
      components: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {}), ...htmlInterpolations]
    })]
  });
};

const GeneralFaqs = () => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
  className: "flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn",
  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionWhatIsStaking",
      answer: "generalFaqAnswerWhatIsStaking",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionWhatIsDelegating",
      answer: "generalFaqAnswerWhatIsDelegating"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionDelegatingBenefits",
      answer: "generalFaqAnswerDelegatingBenefits"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionDelegatingRisks",
      answer: "generalFaqAnswerDelegatingRisks",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    })]
  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionHowMuchRewards",
      answer: "generalFaqAnswerHowMuchRewards"
    }), " ", /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionCommission",
      answer: "generalFaqAnswerCommision",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionTypeOfInterests",
      answer: "generalFaqAnswerTypeOfInterests",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    })]
  })]
});

const FetchAiFaqs = () => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
  className: "flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn",
  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionWhatIs",
      answer: "fetcAiFaqAnswerWhatIs",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionMigrateTokens",
      answer: "fetcAiFaqAnswerMigrateTokens",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
        href: "https://token-bridge.fetch.ai",
        target: "_blank",
        className: "text-green-600 hover:font-semibold"
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionNativeTokenBenefits",
      answer: "fetcAiFaqAnswerNativeTokenBenefits",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionTokenDifferences",
      answer: "fetcAiFaqAnswerTokenDifferences",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
        href: "https://etherscan.io/address/0xaea46A60368A7bD060eec7DF8CBa43b7EF41Ad85",
        target: "_blank",
        className: "text-green-600 hover:font-semibold break-words"
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionTokenUtility",
      answer: "fetcAiFaqAnswerTokenUtility"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionStakeSwapDate",
      answer: "fetcAiFaqAnswerStakeSwapDate"
    })]
  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetchAiFaqQuestionChooseValidator",
      answer: "fetchAiFaqAnswerChooseValidator",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
        href: "https://explore-fetchhub.fetch.ai/validators?sort=commission&dir=1",
        target: "_blank",
        className: "text-green-600 hover:font-semibold"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("strong", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "generalFaqQuestionCommission",
      answer: "generalFaqAnswerCommision",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("br", {})]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionUnboundingPeriod",
      answer: "fetcAiFaqAnswerUnboundingPeriod"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionRedelegatingPeriod",
      answer: "fetcAiFaqAnswerRedelegatingPeriod"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "fetcAiFaqQuestionStakeSwapInfo",
      answer: "fetcAiFaqAnswerStakeSwapInfo",
      htmlInterpolations: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("a", {
        href: "https://fetch.ai/the-fetch-ai-stake-swap-frequently-asked-questions",
        target: "_blank",
        className: "text-green-600 hover:font-semibold"
      })]
    })]
  })]
});

const CartesiFaqs = () => /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
  className: "flex flex-col lg:flex-row lg:space-x-12 w-full animate-fadeIn",
  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "cartesiFaqQuestionWhatIs",
      answer: "cartesiFaqAnswerWhatIs"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "cartesiFaqQuestionTokenUtility",
      answer: "cartesiFaqAnswerTokenUtility"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "cartesiFaqQuestionDelegationAvailability",
      answer: "cartesiFaqAnswerDelegationAvailability"
    })]
  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "lg:w-5/6",
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "cartesiFaqQuestionDelegationAvailability",
      answer: "cartesiFaqAnswerDelegationAvailability"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FaqCard, {
      question: "cartesiFaqQuestionFurtherInfo",
      answer: "cartesiFaqAnswerFurtherInfo"
    })]
  })]
});

const FAQS = {
  general: {},
  fetchAi: {},
  cartesi: {}
};

const Button = ({
  label,
  active,
  onClickHandler = () => {}
}) => {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default()("home");
  const cssClasses = active ? "cursor-auto border focus:ring-1 bg-green-600 text-white focus:ring-0" : "text-gray-500 hover:bg-gray-500 hover:text-white";
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("button", {
    onClick: () => onClickHandler(label),
    className: `px-4 py-1 mx-2 mt-2 text-sm font-medium transition-colors duration-200 transform
        rounded-full md:mt-0 focus:outline-none
        ${cssClasses}`,
    children: t(`faqs.${label}TabLabel`)
  });
};

function Faqs() {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default()("home");
  const {
    0: selectedTab,
    1: setSelectedTab
  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)("general");

  const Component = props => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("h3", _objectSpread(_objectSpread({}, props), {}, {
    className: "text-2xl font-medium text-gray-500 uppercase md:text-4xl"
  }));

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
    className: "flex flex-col items-center pb-24 bg-waves-background bg-cover bg-right-top",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "text-center mt-12",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((next_translate_Trans__WEBPACK_IMPORTED_MODULE_4___default()), {
        i18nKey: "home:faqs.title",
        components: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Component, {}), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("span", {
          className: "text-gray-700"
        })]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "pt-8 pb-12 text-gray-600",
        children: t("faqs.subtitle")
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Fade, {
      triggerOnce: true,
      cascade: true,
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("nav", {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "container px-6 py-4 mx-auto",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
            className: "md:flex md:items-center md:justify-between",
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: "flex-1 md:flex md:items-center md:justify-between",
              children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                className: "flex flex-col -mx-4 md:flex-row md:items-center md:mx-8",
                children: Object.keys(FAQS).map(label => /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(Button, {
                  label: label,
                  active: label === selectedTab,
                  onClickHandler: setSelectedTab
                }, label))
              })
            })
          })
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        children: [selectedTab === "general" && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(GeneralFaqs, {}), selectedTab === "fetchAi" && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(FetchAiFaqs, {}), selectedTab === "cartesi" && /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx(CartesiFaqs, {})]
      })]
    })]
  });
}

/***/ })

};
;