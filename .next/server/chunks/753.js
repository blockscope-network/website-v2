exports.id = 753;
exports.ids = [753];
exports.modules = {

/***/ 116:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "default": function() { return /* binding */ Header; }
});

// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(5282);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(9297);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
// EXTERNAL MODULE: external "next-translate/useTranslation"
var useTranslation_ = __webpack_require__(8841);
var useTranslation_default = /*#__PURE__*/__webpack_require__.n(useTranslation_);
// EXTERNAL MODULE: external "next-translate/setLanguage"
var setLanguage_ = __webpack_require__(7933);
var setLanguage_default = /*#__PURE__*/__webpack_require__.n(setLanguage_);
;// CONCATENATED MODULE: ./i18n.json
var i18n_namespaceObject = JSON.parse('{"locales":["es","en"],"defaultLocale":"en","pages":{"*":["common","layout"],"/":["home"]}}');
// EXTERNAL MODULE: ./pages/components/layout/layout.module.css
var layout_module = __webpack_require__(3205);
var layout_module_default = /*#__PURE__*/__webpack_require__.n(layout_module);
;// CONCATENATED MODULE: ./pages/components/layout/header.js







const {
  locales
} = i18n_namespaceObject;

function Header() {
  const {
    t,
    lang
  } = useTranslation_default()("layout");
  const {
    0: collapsed,
    1: setCollapsed
  } = (0,external_react_.useState)(true);
  const collapsedClass = collapsed ? "hidden" : "";
  console.log((layout_module_default()).header);
  return /*#__PURE__*/jsx_runtime_.jsx("nav", {
    className: `${(layout_module_default()).header} fixed top-0 bg-white w-full border-b-2 border-pink-600 bg-opacity-90 z-50 h`,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: "px-10 py-4 lg:flex lg:justify-between lg:items-center",
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: "flex items-center justify-between",
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: "pr-8",
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            src: "/img/logos/blockscope_logo_letters_800.png",
            width: "278px",
            alt: "Blockscope logo",
            style: {
              maxWidth: "none"
            }
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex lg:hidden",
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: `${(layout_module_default()).headerFlags} flex justify-center lg:hidden mr-6 mt-1`,
            children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "px-4 mt-1",
              onClick: async () => await setLanguage_default()("en"),
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                src: "/img/flags/gb.svg",
                width: "25px",
                height: "20px",
                className: " cursor-pointer"
              })
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: "px-4 mt-1",
              onClick: async () => await setLanguage_default()("es"),
              children: /*#__PURE__*/jsx_runtime_.jsx("img", {
                src: "/img/flags/es.svg",
                width: "25px",
                height: "20px",
                className: " cursor-pointer"
              })
            })]
          }), /*#__PURE__*/jsx_runtime_.jsx("button", {
            type: "button",
            className: "text-gray-500 hover:text-gray-600 focus:outline-none focus:text-gray-600",
            "aria-label": "toggle menu",
            onClick: () => setCollapsed(!collapsed),
            children: /*#__PURE__*/jsx_runtime_.jsx("svg", {
              viewBox: "0 0 24 24",
              className: "w-8 h-8 fill-current",
              children: /*#__PURE__*/jsx_runtime_.jsx("path", {
                fillRule: "evenodd",
                d: "M4 5h16a1 1 0 0 1 0 2H4a1 1 0 1 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2zm0 6h16a1 1 0 0 1 0 2H4a1 1 0 0 1 0-2z"
              })
            })
          })]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: `items-center lg:flex ${collapsedClass}`,
        children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex flex-col lg:flex-row lg:mx-18 pr-4 content-center justify-content-center text-m text-gray-700",
          children: [/*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "#home",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.home")
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "#services",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.services")
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "#calculator",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.earnings")
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "#faq",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.faq")
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "https://blockscope.medium.com/",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              target: "_blank",
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.blog")
            })
          }), /*#__PURE__*/jsx_runtime_.jsx(next_link.default, {
            href: "https://medium.com/fetch-ai/how-to-stake-on-mainnet-2-using-cosmostation-wallet-e7e1a4e0ba23",
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              target: "_blank",
              className: "my-1 hover:text-pink-600 lg:mx-4 lg:my-1 py-4",
              onClick: () => !collapsed && setCollapsed(true),
              children: t("header.stake")
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: "flex flex-row justify-center lg:flex hidden",
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "px-4 mt-1",
            onClick: async () => await setLanguage_default()("en"),
            children: /*#__PURE__*/jsx_runtime_.jsx("img", {
              src: "/img/flags/gb.svg",
              width: "20px",
              height: "16px",
              className: " cursor-pointer"
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: "px-4 mt-1 cursor-pointer",
            onClick: async () => await setLanguage_default()("es"),
            children: /*#__PURE__*/jsx_runtime_.jsx("img", {
              src: "/img/flags/es.svg",
              width: "20px",
              height: "16px",
              className: " cursor-pointer"
            })
          })]
        })]
      })]
    })
  });
}

/***/ }),

/***/ 3205:
/***/ (function(module) {

// Exports
module.exports = {
	"container": "layout_container__3OD7W",
	"content": "layout_content__cNcHH",
	"header": "layout_header__2HOwm",
	"headerFlags": "layout_headerFlags__3PFx8",
	"logo": "layout_logo__-CNat",
	"grid": "layout_grid__26oaB"
};


/***/ }),

/***/ 4453:
/***/ (function() {

/* (ignored) */

/***/ })

};
;