exports.id = 944;
exports.ids = [944];
exports.modules = {

/***/ 3944:
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": function() { return /* binding */ technology; }
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5282);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_tsparticles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6810);
/* harmony import */ var react_tsparticles__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_tsparticles__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(104);
/* harmony import */ var react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8841);
/* harmony import */ var next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _technology_module_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(2644);
/* harmony import */ var _technology_module_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_technology_module_css__WEBPACK_IMPORTED_MODULE_4__);







const particlesInit = () => {};

const particlesLoaded = () => {};

const particlesOptions = {
  autoPlay: true,
  particles: {
    number: {
      value: 90,
      density: {
        enable: true,
        value_area: 800
      }
    },
    color: {
      value: "#9e1068",
      animation: {
        h: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false
        },
        s: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false
        },
        l: {
          count: 1,
          enable: true,
          offset: 0,
          speed: 1,
          sync: false
        }
      }
    },
    opacity: {
      value: 0.3,
      random: true,
      anim: {
        enable: true,
        speed: 10,
        opacity_min: 0.1,
        sync: false
      }
    },
    size: {
      value: 4,
      random: true,
      anim: {
        enable: false,
        speed: 2,
        size_min: 0.1,
        sync: false
      }
    },
    line_linked: {
      enable: true,
      distance: 179,
      color: "#c41d7f",
      opacity: 0.5,
      width: 1
    },
    move: {
      enable: true,
      speed: 0.5,
      direction: "none",
      random: false,
      straight: false,
      out_mode: "out",
      bounce: false,
      attract: {
        enable: false,
        rotateX: 600,
        rotateY: 1200
      }
    }
  },
  retina_detect: true
};
function technology() {
  const {
    t,
    lang
  } = next_translate_useTranslation__WEBPACK_IMPORTED_MODULE_3___default()("home");
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("section", {
    className: `relative ${(_technology_module_css__WEBPACK_IMPORTED_MODULE_4___default().outerSpace)}`,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "text-center absolute top-0 inset-0 mt-12",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("h3", {
        className: "text-2xl font-medium text-gray-400 uppercase md:text-4xl",
        children: [t("technology.title"), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "pt-6 px-6 flex flex-col items-center",
          children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
            src: "/img/logos/blockscope_logo_only-letters_white.svg",
            width: "382px",
            height: "37px"
          })
        })]
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
        className: "mt-12 text-xl text-gray-300",
        children: t("technology.subtitle")
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      id: "particles-js",
      className: "mx-auto md:h-128 h-192",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx((react_tsparticles__WEBPACK_IMPORTED_MODULE_1___default()), {
        id: "tsparticlesStars",
        init: particlesInit,
        loaded: particlesLoaded,
        options: particlesOptions,
        className: (_technology_module_css__WEBPACK_IMPORTED_MODULE_4___default().canvasNetwork)
      })
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
      className: "container px-6 mx-auto absolute top-72 inset-0",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
        className: "w-full flex flex-col items-center mt-2",
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
          className: "flex flex-col space-y-16 lg:space-y-0 lg:space-x-16 lg:flex-row lg:content-around",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Zoom, {
            triggerOnce: true,
            cascade: true,
            damping: "0.2",
            duration: "600",
            children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: `max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${(_technology_module_css__WEBPACK_IMPORTED_MODULE_4___default().logoShadow)}`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                  className: "flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-pink-500 via-red-500 to-pink-700",
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    src: "/img/logos/blockscope_logo_no-letters_white_solid_200.png",
                    width: "40px",
                    height: "44px"
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Fade, {
                  triggerOnce: true,
                  cascade: true,
                  damping: "0.2",
                  delay: 200,
                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "font-bold text-center text-xl text-gray-500 mt-6",
                    children: t("technology.performanceCardTitle")
                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8",
                    children: t("technology.performanceCardParagraph")
                  })]
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: `max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${(_technology_module_css__WEBPACK_IMPORTED_MODULE_4___default().logoShadowGreen)}`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                  className: "flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-green-400 via-green-600 bg-green-700",
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    src: "/img/logos/blockscope_logo_no-letters_white_solid_200.png",
                    width: "40px",
                    height: "44px"
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Fade, {
                  triggerOnce: true,
                  cascade: true,
                  damping: "0.2",
                  delay: 200,
                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "font-bold text-center text-xl text-gray-500 mt-6",
                    children: t("technology.securityCardTitle")
                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8",
                    children: t("technology.securityCardParagraph")
                  })]
                })]
              })
            }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
              className: `max-w-sm rounded-xl overflow-hidden shadow-lg bg-white ${(_technology_module_css__WEBPACK_IMPORTED_MODULE_4___default().logoShadow)}`,
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
                children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("div", {
                  className: "flex justify-center text-center rounded-t-xl pt-4 pb-2 bg-gradient-to-r from-pink-500 via-red-500 to-pink-700",
                  children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("img", {
                    src: "/img/logos/blockscope_logo_no-letters_white_solid_200.png",
                    width: "40px",
                    height: "44px"
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(react_awesome_reveal__WEBPACK_IMPORTED_MODULE_2__.Fade, {
                  triggerOnce: true,
                  cascade: true,
                  damping: "0.2",
                  delay: 200,
                  children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "font-bold text-center text-xl text-gray-500 mt-6",
                    children: t("technology.experienceCardTitle")
                  }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx("p", {
                    className: "xs:text-justify text-gray-500 text-base mx-12 mt-4 mb-8",
                    children: t("technology.experienceCardParagraph")
                  })]
                })]
              })
            })]
          })
        })
      })
    })]
  });
}

/***/ }),

/***/ 2644:
/***/ (function(module) {

// Exports
module.exports = {
	"canvasNetwork": "technology_canvasNetwork__1wELn",
	"outerSpace": "technology_outerSpace__VCzs3",
	"logoShadow": "technology_logoShadow__2V6-J",
	"logoShadowGreen": "technology_logoShadowGreen__3Omo4"
};


/***/ })

};
;